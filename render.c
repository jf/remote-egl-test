#include <epoxy/egl.h>
#include <epoxy/gl.h>
#include <fcntl.h>
#include <gio/gio.h>
#include <gio/gunixfdlist.h>

#include "egl_errors.h"

#define APP_NAME "org.example.RemoteEgl"

typedef struct
{
  GAsyncQueue *queue;
  GDBusConnection *conn;
  GMainLoop *loop;
} Client;

typedef struct
{
  EGLDisplay display;
  EGLContext context;
  gint64 start_time;
  int width;
  int height;
  GLint time_uniform;
  GLint screen_size_uniform;
  gboolean pending: 1;
  gboolean quit: 1;
  gboolean debug_frame: 1;
} RenderState;

static void
free_render_state (RenderState *state)
{
  if (state->context != EGL_NO_CONTEXT)
    eglDestroyContext (state->display, state->context);
  if (state->display)
    eglTerminate (state->display);
}

static void
handle_message (RenderState *state, GDBusMessage *message_)
{
  g_autoptr (GDBusMessage) message = message_;
  const char *method = g_dbus_message_get_member (message);

  if (g_strcmp0 (method, "Tick") == 0)
    {
      GVariant *body = g_dbus_message_get_body (message);
      g_variant_get (body, "(ii)", &state->width, &state->height);
      state->pending = TRUE;
    }
  else if (g_strcmp0 (method, "Quit") == 0)
    {
      state->quit = TRUE;
    }
}

static const char VERT_SRC[] = "\
precision mediump float;\n\
attribute vec2 pos;\n\
void main(void) {\n\
  gl_Position = vec4(pos, 0, 1);\n\
}\
";
static const char FRAG_SRC[] = "\
precision mediump float;\n\
uniform float time;\n\
uniform vec2 screen_size;\n\
void main(void) {\n\
  vec2 uv = gl_FragCoord.xy / screen_size;\n\
  vec3 col = .5 + .5 * cos(time + uv.xyx + vec3(0, 2, 4));\n\
  gl_FragColor = vec4(col, 1);\n\
}\
";

static GDBusMessage *
render (RenderState *state)
{
  GVariantBuilder *planes;
  GLuint tex;
  EGLImage image;
  int fourcc, num_planes;
  EGLuint64KHR modifiers;
  GDBusMessage *message;
  g_autoptr (GUnixFDList) fd_list = NULL;

  glGenTextures (1, &tex);
  glBindTexture (GL_TEXTURE_2D, tex);
  glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, state->width, state->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  glRenderbufferStorage (GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, state->width, state->height);
  glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

  {
    GLfloat screen_size[2] = { state->width, state->height };
    gint64 cur_time = g_get_monotonic_time ();
    if (state->start_time < 0)
      state->start_time = cur_time;

    glUniform2fv (state->screen_size_uniform, 1, screen_size);
    glUniform1f (state->time_uniform, ((double) (cur_time - state->start_time)) / 1000000.);
  }

  glViewport (0, 0, state->width, state->height);
  glDrawArrays (GL_TRIANGLES, 0, 6);
  glFinish ();

  if (G_UNLIKELY (state->debug_frame))
    {
      size_t stride = state->width * 4;
      g_autofree GLubyte *pixels = g_new (GLubyte, state->height * stride);

      glReadPixels (0, 0, state->width, state->height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
      for (size_t y = 0; y < state->height; y++)
        {
          for (size_t x = 0; x < state->width; x++)
            {
              size_t off = x * 4 + y * stride;
              GLubyte r = pixels[off + 0];
              GLubyte g = pixels[off + 1];
              GLubyte b = pixels[off + 2];
              g_print ("\x1b[48;2;%d;%d;%dm ", r, g, b);
            }
          g_print ("\x1b[0m\n");
        }
      return NULL;
    }

  image = eglCreateImageKHR (state->display,
                             state->context,
                             EGL_GL_TEXTURE_2D,
                             (EGLClientBuffer) (GLintptr) tex,
                             NULL);
  if (image == EGL_NO_IMAGE)
    g_error ("eglCreateImageKHR: %s", egl_error_str ());

  if (!eglExportDMABUFImageQueryMESA (state->display, image, &fourcc, &num_planes, &modifiers))
    g_error ("eglExportDMABUFImageQueryMESA: %s", egl_error_str ());

  int fds[num_planes];
  EGLint strides[num_planes];
  EGLint offsets[num_planes];

  if (!eglExportDMABUFImageMESA (state->display, image, fds, strides, offsets))
    g_error ("eglExportDMABUFImageQueryMESA: %s", egl_error_str ());

  glBindTexture (GL_TEXTURE_2D, 0);
  glDeleteTextures (1, &tex);

  message = g_dbus_message_new_method_call (APP_NAME,
                                            "/org/example/RemoteEgl",
                                            APP_NAME,
                                            "Frame");
  planes = g_variant_builder_new (G_VARIANT_TYPE ("a(huu)"));
  for (int i = 0; i < num_planes; i++)
    g_variant_builder_add (planes, "(huu)", i, strides[i], offsets[i]);
  g_dbus_message_set_body (message,  g_variant_new ("(uuuta(huu))",
                                                    state->width,
                                                    state->height,
                                                    fourcc,
                                                    modifiers,
                                                    planes));
  fd_list = g_unix_fd_list_new_from_array (fds, num_planes);
  g_dbus_message_set_unix_fd_list (message, fd_list);

  eglDestroyImage (state->display, image);

  return message;
}

static inline void
compile_shader (GLuint shader, const char *name, const char *src)
{
  GLint status;

  glShaderSource (shader, 1, (const char *[]) { src }, NULL);
  glCompileShader (shader);
  glGetShaderiv (shader, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE)
    {
      GLint len;
      g_autofree char *log = NULL;

      glGetShaderiv (shader, GL_INFO_LOG_LENGTH, &len);
      log = g_malloc (len);
      glGetShaderInfoLog (shader, len, NULL, log);
      g_error ("Error compiling %s:\n%s", name, log);
    }
}

static gpointer
render_thread (gpointer user_data)
{
  Client *client = user_data;
  _GLIB_CLEANUP (free_render_state) RenderState state = { 0, };
  int major, minor;
  const EGLint config_attribs[] = {
    EGL_BIND_TO_TEXTURE_RGBA, GL_TRUE,
    EGL_SURFACE_TYPE,         EGL_PBUFFER_BIT,
    EGL_RED_SIZE,             8,
    EGL_GREEN_SIZE,           8,
    EGL_BLUE_SIZE,            8,
    EGL_ALPHA_SIZE,           8,
    EGL_DEPTH_SIZE,           0,
    EGL_CONFORMANT,           EGL_OPENGL_ES2_BIT,
    EGL_RENDERABLE_TYPE,      EGL_OPENGL_ES2_BIT,
    EGL_NATIVE_RENDERABLE,    EGL_TRUE,
    EGL_NONE,
  };
  EGLConfig config;
  EGLint num_configs;
  const EGLint ctx_attribs[] = {
    EGL_CONTEXT_MAJOR_VERSION, 2,
    EGL_NONE,
  };
  GLuint fb, rb, buf, prog, vs, fs;
  GLint pos;

  if (!epoxy_has_egl_extension (state.display, "EGL_MESA_platform_surfaceless"))
    g_error ("Missing extension: EGL_MESA_platform_surfaceless");
  state.display = eglGetPlatformDisplayEXT (EGL_PLATFORM_SURFACELESS_MESA, NULL, NULL);
  if (!state.display)
    g_error ("eglGetPlatformDisplayEXT: %s", egl_error_str ());
  if (!eglInitialize (state.display, &major, &minor))
    g_error ("eglInitialize: %s", egl_error_str ());
  if (major < 1 || (major == 1 && minor < 5))
    g_error ("Needed EGL 1.5, got EGL %d.%d", major, minor);
  if (!epoxy_has_egl_extension (state.display, "EGL_KHR_image_base"))
    g_error ("Missing extension: EGL_KHR_image_base");
  if (!epoxy_has_egl_extension (state.display, "EGL_MESA_image_dma_buf_export"))
    g_error ("Missing extension: EGL_MESA_image_dma_buf_export");
  if (!eglChooseConfig (state.display, config_attribs, &config, 1, &num_configs))
    g_error ("eglChooseConfig: %s", egl_error_str ());
  if (num_configs < 1)
    g_error ("eglChooseConfig: no valid config");
  state.context = eglCreateContext (state.display, config, EGL_NO_CONTEXT, ctx_attribs);
  if (state.context == EGL_NO_CONTEXT)
    g_error ("eglCreateContext: %s", egl_error_str ());
  if (!eglMakeCurrent (state.display, NULL, NULL, state.context))
    g_error ("eglMakeCurrent: %s", egl_error_str ());
  if (!eglBindAPI (EGL_OPENGL_ES_API))
    g_error ("eglBindAPI: %s", egl_error_str ());

  glGenFramebuffers (1, &fb);
  glGenRenderbuffers (1, &rb);
  glGenBuffers (1, &buf);
  prog = glCreateProgram ();
  vs = glCreateShader (GL_VERTEX_SHADER);
  fs = glCreateShader (GL_FRAGMENT_SHADER);

  glBindBuffer (GL_ARRAY_BUFFER, buf);
  const GLfloat data[] = { -1.f, -1.f,  1.f, -1.f, -1.f,  1.f,
                           -1.f,  1.f,  1.f, -1.f,  1.f,  1.f };
  glBufferData (GL_ARRAY_BUFFER, sizeof (data), data, GL_STREAM_DRAW);

  glBindFramebuffer (GL_FRAMEBUFFER, fb);
  glBindRenderbuffer (GL_RENDERBUFFER, rb);
  glFramebufferRenderbuffer (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb);

  compile_shader (vs, "vertex shader", VERT_SRC);
  compile_shader (fs, "fragment shader", FRAG_SRC);

  glAttachShader (prog, vs);
  glAttachShader (prog, fs);
  glLinkProgram (prog);
  glUseProgram (prog);

  pos = glGetAttribLocation (prog, "pos");
  glVertexAttribPointer (pos, 2, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray (pos);

  state.time_uniform = glGetUniformLocation (prog, "time");
  state.screen_size_uniform = glGetUniformLocation (prog, "screen_size");

  glDeleteShader (vs);
  glDeleteShader (fs);

  state.debug_frame = client->conn == NULL;
  state.start_time = -1;

  while (TRUE)
    {
      g_autoptr (GDBusMessage) message = g_async_queue_pop (client->queue);

      handle_message (&state, g_steal_pointer (&message));

      /* pull off any other pending messages */
      while (TRUE)
        {
          message = g_async_queue_try_pop (client->queue);
          if (message)
            handle_message (&state, g_steal_pointer (&message));
          else
            break;
        }

      if (state.quit)
        break;

      if (state.pending)
        {
          g_autoptr (GError) error = NULL;
          g_autoptr (GDBusMessage) message = NULL;
          gboolean status;

          message = render (&state);
          if (client->conn && message)
            {
              if (g_dbus_connection_is_closed (client->conn))
                break;
              status = g_dbus_connection_send_message (client->conn,
                                                       message,
                                                       G_DBUS_SEND_MESSAGE_FLAGS_NONE,
                                                       NULL,
                                                       &error);
              if (!status)
                g_error ("%s\n", error->message);
            }

          if (state.debug_frame)
            break;
        }
    }

  glDeleteFramebuffers (1, &fb);
  glDeleteRenderbuffers (1, &rb);
  glDeleteBuffers (1, &buf);
  glDeleteProgram (prog);

  g_main_loop_quit (client->loop);
  return NULL;
}

static GDBusMessage *
dbus_filter (GDBusConnection *connection,
             GDBusMessage    *message_,
             gboolean         incoming,
             gpointer         user_data)
{
  Client *client = user_data;
  const char *method;
  g_autoptr (GDBusMessage) message = message_;

  if (!incoming)
    return g_steal_pointer (&message);

  method = g_dbus_message_get_member (message);

  if (g_strcmp0 (method, "Tick") == 0)
    {
      GVariant *body = g_dbus_message_get_body (message);

      if (!body || !g_variant_is_of_type (body, G_VARIANT_TYPE ("(ii)")))
        {
          g_warning ("Invalid Tick body");
          return NULL;
        }

      g_async_queue_push (client->queue, g_steal_pointer (&message));
    }
  else if (g_strcmp0 (method, "Quit") == 0)
    {
      g_async_queue_push_front (client->queue, g_steal_pointer (&message));
    }

  return NULL;
}

static void
dbus_closed (GDBusConnection *connection,
             gboolean         remote_peer_vanished,
             GError          *error,
             gpointer         user_data)
{
  Client *client = user_data;
  GDBusMessage *message;

  if (error)
    g_critical ("socket connection closed: %s", error->message);
  else
    g_critical ("socket connection closed");

  message = g_dbus_message_new_method_call (APP_NAME "Renderer",
                                            "/org/example/RemoteEglRenderer",
                                            APP_NAME "Renderer",
                                            "Quit");
  g_async_queue_push (client->queue, message);
}

static GLogWriterOutput
write_log (GLogLevelFlags   log_level,
           const GLogField *fields,
           gsize            n_fields,
           gpointer         user_data)
{
  const char *message = NULL;

  for (int i = 0; i < n_fields; i++)
    {
      const GLogField *field = &fields[i];

      if (g_strcmp0 (field->key, "MESSAGE") == 0)
        {
          message = field->value;
          break;
        }
    }

  if (!message)
    return G_LOG_WRITER_UNHANDLED;

  if (log_level & (G_LOG_FLAG_FATAL|G_LOG_LEVEL_ERROR|G_LOG_LEVEL_CRITICAL))
    g_printerr ("%s\n", message);
  else
    g_print ("%s\n", message);

  return G_LOG_WRITER_HANDLED;
}

const int SOCKET_FD = 3;

int
main (int argc, char *argv[])
{
  g_autoptr (GMainLoop) loop = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GSocket) socket = NULL;
  g_autoptr (GSocketConnection) conn = NULL;
  g_autoptr (GDBusConnection) dbus = NULL;
  g_autoptr (GAsyncQueue) queue = NULL;
  g_autoptr (GThread) thread = NULL;
  Client client = { 0, };
  gboolean debug_frame = FALSE;

  queue = g_async_queue_new_full (g_object_unref);
  client.queue = queue;

  for (int i = 1; i < argc; i++)
    {
      if (g_strcmp0 (argv[i], "--debug-frame") == 0)
        debug_frame = TRUE;
    }

  if (debug_frame)
    {
      GDBusMessage *message;

      message = g_dbus_message_new_method_call (APP_NAME "Renderer",
                                                "/org/example/RemoteEglRenderer",
                                                APP_NAME "Renderer",
                                                "Tick");
      g_dbus_message_set_body (message, g_variant_new ("(ii)", 16, 16));
      g_async_queue_push (client.queue, message);
    }
  else
    {
      g_log_set_writer_func (write_log, NULL, NULL);

      if (fcntl (SOCKET_FD, F_GETFD) < 0)
        g_error ("fd %d not open", SOCKET_FD);

      socket = g_socket_new_from_fd (SOCKET_FD, &error);
      if (!socket)
        g_error ("%s\n", error->message);

      conn = g_socket_connection_factory_create_connection (socket);
      dbus = g_dbus_connection_new_sync (G_IO_STREAM (conn),
                                         NULL,
                                         G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING
                                           | G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT,
                                         NULL,
                                         NULL,
                                         &error);
      if (!dbus)
        g_error ("%s\n", error->message);

      g_signal_connect (dbus, "closed", G_CALLBACK (dbus_closed), &client);

      client.conn = dbus;
      g_dbus_connection_add_filter (dbus, dbus_filter, &client, NULL);
    }

  loop = g_main_loop_new (NULL, FALSE);
  client.loop = loop;
  thread = g_thread_new ("render", render_thread, &client);

  if (client.conn)
    g_dbus_connection_start_message_processing (client.conn);

  g_main_loop_run (loop);
  g_clear_pointer (&thread, g_thread_join);

  return 0;
}
