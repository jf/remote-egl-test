#include <X11/Xlib-xcb.h>
#include <drm_fourcc.h>
#include <epoxy/egl.h>
#include <epoxy/gl.h>
#include <epoxy/glx.h>
#include <gdk/wayland/gdkwayland.h>
#include <gdk/x11/gdkx.h>
#include <gio/gunixfdlist.h>
#include <gtk/gtk.h>
#include <sys/socket.h>
#include <xcb/dri3.h>
#include <xcb/glx.h>

#include "egl_errors.h"

#define GUID "34e52dafc1c075ae06e49de762a4e1d8"
#define APP_NAME "org.example.RemoteEgl"

static const char *exe_dir = NULL;

#define REMOTE_EGL_WIDGET_TYPE (remote_egl_widget_get_type ())
G_DECLARE_FINAL_TYPE (RemoteEglWidget, remote_egl_widget, REMOTE, EGL_WIDGET, GtkWidget)

struct _RemoteEglWidget
{
  GtkWidget        parent;

  GSubprocess     *renderer;
  GDBusConnection *dbus_conn;
  GDBusMessage    *message;
  GtkWidget       *label;
  GdkGLContext    *context;
  GdkTexture      *texture;
  GskGLShader     *swap_shader;
  EGLDisplay       egl_display;
  Display         *x11_display;
  GLXFBConfig      x11_fb_config;
  int              x11_screen;
  int              x11_fb_depth;
  guint            tick_id;
  gboolean         supports_modifiers: 1;
  gboolean         swap_rb: 1;
};

G_DEFINE_TYPE (RemoteEglWidget, remote_egl_widget, GTK_TYPE_WIDGET);

static void remote_egl_widget_update_image (RemoteEglWidget *ewidget);

static void
remote_egl_widget_set_error (RemoteEglWidget *ewidget, GError *error)
{
  if (!ewidget->label)
    {
      GtkWidget *scroll = gtk_scrolled_window_new ();
      GtkWidget *child;

      while ((child = gtk_widget_get_first_child (GTK_WIDGET (ewidget))) != NULL)
        gtk_widget_unparent (child);

      ewidget->label = gtk_label_new (NULL);
      gtk_label_set_justify (GTK_LABEL (ewidget->label), GTK_JUSTIFY_CENTER);
      gtk_label_set_selectable (GTK_LABEL (ewidget->label), TRUE);
      gtk_label_set_wrap (GTK_LABEL (ewidget->label), TRUE);
      gtk_scrolled_window_set_child (GTK_SCROLLED_WINDOW (scroll), ewidget->label);
      gtk_widget_set_parent (scroll, GTK_WIDGET (ewidget));
    }

  g_critical ("%s", error->message);
  gtk_label_set_label (GTK_LABEL (ewidget->label), error->message);
  g_error_free (error);
  if (ewidget->tick_id)
    {
      gtk_widget_remove_tick_callback (GTK_WIDGET (ewidget), ewidget->tick_id);
      ewidget->tick_id = 0;
    }
}

static void G_GNUC_PRINTF (2, 3)
remote_egl_widget_set_error_literal (RemoteEglWidget *ewidget,
                                     const char *fmt,
                                     ...)
{
  va_list args;
  GError *error;

  va_start (args, fmt);
  error = g_error_new_valist (GDK_GL_ERROR, GDK_GL_ERROR_NOT_AVAILABLE, fmt, args);
  remote_egl_widget_set_error (ewidget, error);
  va_end (args);
}

static void
remote_egl_widget_init (RemoteEglWidget *ewidget)
{
  ewidget->x11_fb_config = NULL;

  GtkWidget *spinner = gtk_spinner_new ();

  gtk_widget_set_halign (spinner, GTK_ALIGN_CENTER);
  gtk_widget_set_valign (spinner, GTK_ALIGN_CENTER);
  gtk_spinner_set_spinning (GTK_SPINNER (spinner), TRUE);
  gtk_widget_set_parent (spinner, GTK_WIDGET (ewidget));
}

static gboolean
tick (GtkWidget* widget, GdkFrameClock* frame_clock, gpointer user_data)
{
  RemoteEglWidget *ewidget = REMOTE_EGL_WIDGET (widget);

  if (ewidget->dbus_conn)
    {
      int width = gtk_widget_get_width (widget);
      int height = gtk_widget_get_height (widget);

      if (width && height)
        g_dbus_connection_call (ewidget->dbus_conn,
                                APP_NAME "Renderer",
                                "/org/example/RemoteEglRenderer",
                                APP_NAME "Renderer",
                                "Tick",
                                g_variant_new ("(ii)", width, height),
                                NULL,
                                G_DBUS_CALL_FLAGS_NONE,
                                -1,
                                NULL,
                                NULL,
                                NULL);
    }
  return TRUE;
}

static GDBusMessage *
dbus_filter (GDBusConnection *connection,
             GDBusMessage    *message_,
             gboolean         incoming,
             gpointer         user_data)
{
  g_autoptr (GDBusMessage) message = message_;
  GWeakRef *weak = user_data;
  RemoteEglWidget *ewidget = REMOTE_EGL_WIDGET (g_weak_ref_get (weak));
  const char *method;

  if (!incoming)
    return g_steal_pointer (&message);
  if (!ewidget)
    return NULL;

  method = g_dbus_message_get_member (message);

  if (g_strcmp0 (method, "Frame") == 0)
    {
      GVariant *body = g_dbus_message_get_body (message);
      if (!body || !g_variant_is_of_type (body, G_VARIANT_TYPE ("(uuuta(huu))")))
        {
          g_warning ("Invalid Frame body");
          return NULL;
        }
      g_set_object (&ewidget->message, g_steal_pointer (&message));

      if (!ewidget->label)
        {
          GtkWidget *child;

          while ((child = gtk_widget_get_first_child (GTK_WIDGET (ewidget))) != NULL)
            gtk_widget_unparent (child);
        }
      gtk_widget_queue_draw (GTK_WIDGET (ewidget));
    }

  return NULL;
}

static void
free_weak_ref (gpointer data)
{
  g_autofree GWeakRef *weak = data;

  g_weak_ref_clear (weak);
}

static void
dbus_connected (GObject *source_object,
                GAsyncResult *res,
                gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GDBusConnection) conn = NULL;
  g_autoptr (RemoteEglWidget) ewidget = REMOTE_EGL_WIDGET (user_data);

  conn = g_dbus_connection_new_finish (res, &error);
  if (conn)
    {
      GWeakRef *weak = g_new (GWeakRef, 1);

      g_set_object (&ewidget->dbus_conn, conn);
      g_weak_ref_init (weak, ewidget);
      g_dbus_connection_add_filter (conn, dbus_filter, weak, free_weak_ref);
      g_dbus_connection_start_message_processing (conn);
    }
  else
    {
      g_clear_object (&ewidget->dbus_conn);
      remote_egl_widget_set_error (ewidget, g_steal_pointer (&error));
    }
}

static void
free_fds (int (*fds)[2])
{
  for (size_t i = 0; i < G_N_ELEMENTS (*fds); i++)
    if ((*fds)[i] != -1)
      close ((*fds)[i]);
}

static void
renderer_exited (GObject *source_object,
                 GAsyncResult *res,
                 gpointer user_data)
{
  GSubprocess *renderer = G_SUBPROCESS (source_object);
  g_autofree GWeakRef *weak = user_data;
  RemoteEglWidget *ewidget = g_weak_ref_get (weak);
  g_autoptr (GError) error = NULL;
  g_autofree char *errstr = NULL;
  const char *curerr = NULL;

  if (!g_subprocess_communicate_utf8_finish (renderer, res, NULL, &errstr, &error))
    curerr = error->message;
  else if (errstr && errstr[0])
    curerr = g_strstrip (errstr);
  else if (!g_subprocess_wait_check (renderer, NULL, &error))
    curerr = error->message;

  if (curerr)
    {
      if (ewidget)
        {
          if (error)
            remote_egl_widget_set_error (ewidget, g_steal_pointer (&error));
          else
            remote_egl_widget_set_error_literal (ewidget, "%s", curerr);
        }
      else
        {
          g_critical ("%s", curerr);
        }
    }
  if (ewidget)
    {
      g_clear_object (&ewidget->dbus_conn);
      g_clear_object (&ewidget->renderer);
    }

  g_weak_ref_clear (weak);
}

static const char SWAP_SRC[] = "\
uniform sampler2D u_texture1;\n\
void mainImage(out vec4 fragColor, in vec2 fragCoord, in vec2 resolution, in vec2 uv)\n\
{\n\
  fragColor = GskTexture(u_texture1, uv).bgra;\n\
}\
";

static void
remote_egl_widget_realize (GtkWidget *widget)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GSocket) socket = NULL;
  g_autoptr (GSocketConnection) conn = NULL;
  g_autoptr (GSubprocessLauncher) launcher = NULL;
  g_autoptr (GString) missing_exts = NULL;
  _GLIB_CLEANUP (free_fds) int socket_fds[2] = { -1, -1 };
  RemoteEglWidget *ewidget = REMOTE_EGL_WIDGET (widget);
  GdkDisplay *display;
  GdkSurface *surface;
  GWeakRef *ewidget_weak;

  GTK_WIDGET_CLASS (remote_egl_widget_parent_class)->realize (widget);

  display = gtk_widget_get_display (widget);
  surface = gtk_native_get_surface (GTK_NATIVE (gtk_widget_get_root (widget)));
  g_set_object (&ewidget->context, gdk_surface_create_gl_context (surface, &error));
  if (!ewidget->context)
    {
      remote_egl_widget_set_error (ewidget, g_steal_pointer (&error));
      return;
    }
  gdk_gl_context_make_current (ewidget->context);

  if (GDK_IS_X11_DISPLAY (display))
    {
      int major, minor;

      ewidget->egl_display = gdk_x11_display_get_egl_display (display);
      if (!ewidget->egl_display
          && gdk_x11_display_get_glx_version (display, &major, &minor)
          && (major > 1 || (major == 1 && minor >= 3)))
        {
          GdkX11Screen *screen;

          ewidget->x11_display = gdk_x11_display_get_xdisplay (display);
          screen = gdk_x11_display_get_screen (display);
          ewidget->x11_screen = gdk_x11_screen_get_screen_number (screen);
        }
    }
  else if (GDK_IS_WAYLAND_DISPLAY (display))
    ewidget->egl_display = gdk_wayland_display_get_egl_display (display);

  if (ewidget->egl_display == EGL_NO_DISPLAY && !ewidget->x11_display)
    {
      remote_egl_widget_set_error_literal (ewidget,
                                           "No EGL 1.4 or GLX 1.3 available for %s",
                                           G_OBJECT_TYPE_NAME (display));
      return;
    }

  missing_exts = g_string_new (NULL);
  if (ewidget->egl_display)
    {
      if (!epoxy_has_egl_extension (ewidget->egl_display, "EGL_KHR_image_base"))
        g_string_append (missing_exts, "EGL_KHR_image_base\n");
      if (!epoxy_has_egl_extension (ewidget->egl_display, "EGL_EXT_image_dma_buf_import"))
        g_string_append (missing_exts, "EGL_EXT_image_dma_buf_import\n");
      ewidget->supports_modifiers
        = epoxy_has_egl_extension (ewidget->egl_display, "EGL_EXT_image_dma_buf_import_modifiers");
    }
  else if (ewidget->x11_display)
    {
      int op, ev, err;

      if (!XQueryExtension (ewidget->x11_display, "DRI3", &op, &ev, &err))
        g_string_append (missing_exts, "DRI3 1.2\n");
      else
        {
          xcb_connection_t *conn = XGetXCBConnection (ewidget->x11_display);
          xcb_dri3_query_version_cookie_t cookie;
          g_autofree xcb_dri3_query_version_reply_t *reply = NULL;

          cookie = xcb_dri3_query_version_unchecked (conn, 1, 2);
          reply = xcb_dri3_query_version_reply (conn, cookie, NULL);
          if (!reply || reply->major_version < 1
              || (reply->major_version == 1 && reply->minor_version < 2))
            g_string_append (missing_exts, "DRI3 1.2\n");
        }
      if (!epoxy_has_glx_extension (ewidget->x11_display,
                                    ewidget->x11_screen,
                                    "GLX_EXT_texture_from_pixmap"))
        g_string_append (missing_exts, "GLX_EXT_texture_from_pixmap\n");
    }
  if (!epoxy_has_gl_extension ("GL_OES_EGL_image"))
    g_string_append (missing_exts, "GL_OES_EGL_image\n");

  if (missing_exts->len)
    {
      missing_exts->str[missing_exts->len - 1] = '\0';
      remote_egl_widget_set_error_literal (ewidget,
                                           "Missing extensions:\n%s",
                                           missing_exts->str);
      return;
    }

  if (ewidget->x11_display)
    {
      g_autoptr (GBytes) bytes = g_bytes_new_static (SWAP_SRC, sizeof SWAP_SRC);
      ewidget->swap_shader = gsk_gl_shader_new_from_bytes (bytes);
    }

  if (socketpair (AF_UNIX, SOCK_STREAM, 0, socket_fds) != 0)
    g_error ("socketpair: %s", g_strerror (errno));

  socket = g_socket_new_from_fd (socket_fds[0], &error);
  if (!socket)
    g_error ("%s\n", error->message);

  socket_fds[0] = -1;

  conn = g_socket_connection_factory_create_connection (socket);
  g_dbus_connection_new (G_IO_STREAM (conn),
                         GUID,
                         G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING
                           | G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_SERVER,
                         NULL,
                         NULL,
                         dbus_connected,
                         g_object_ref (ewidget));

  launcher = g_subprocess_launcher_new (G_SUBPROCESS_FLAGS_STDERR_PIPE);
  g_subprocess_launcher_take_fd (launcher, socket_fds[1], 3);
  socket_fds[1] = -1;

  {
    g_autofree char *path_storage = NULL;
    const char *path = "remote-egl-render";

    if (exe_dir)
      {
        path_storage = g_build_filename (exe_dir, path, NULL);
        path = path_storage;
      }
    g_set_object (&ewidget->renderer,
                  g_subprocess_launcher_spawn (launcher, &error, path, NULL));
  }
  if (!ewidget->renderer)
    {
      remote_egl_widget_set_error (ewidget, g_steal_pointer (&error));
      return;
    }

  ewidget_weak = g_new (GWeakRef, 1);
  g_weak_ref_init (ewidget_weak, ewidget);
  g_subprocess_communicate_utf8_async (ewidget->renderer,
                                       NULL,
                                       NULL,
                                       renderer_exited,
                                       ewidget_weak);

  ewidget->tick_id = gtk_widget_add_tick_callback (widget, tick, NULL, NULL);
}

static void
remote_egl_widget_unrealize (GtkWidget *widget)
{
  RemoteEglWidget *ewidget = REMOTE_EGL_WIDGET (widget);

  if (ewidget->tick_id)
    {
      gtk_widget_remove_tick_callback (widget, ewidget->tick_id);
      ewidget->tick_id = 0;
    }

  ewidget->egl_display = EGL_NO_DISPLAY;
  ewidget->x11_display = NULL;
  ewidget->x11_fb_config = NULL;
  ewidget->x11_fb_depth = 0;
  ewidget->swap_rb = FALSE;
  g_clear_object (&ewidget->context);
  g_clear_object (&ewidget->renderer);
  g_clear_object (&ewidget->swap_shader);

  if (ewidget->dbus_conn)
    {
      g_dbus_connection_call (ewidget->dbus_conn,
                              APP_NAME "Renderer",
                              "/org/example/RemoteEglRenderer",
                              APP_NAME "Renderer",
                              "Quit",
                              NULL,
                              NULL,
                              G_DBUS_CALL_FLAGS_NONE,
                              -1,
                              NULL,
                              NULL,
                              NULL);
      g_clear_object (&ewidget->dbus_conn);
    }

  GTK_WIDGET_CLASS (remote_egl_widget_parent_class)->unrealize (widget);
}

static void
remote_egl_widget_size_allocate (GtkWidget *widget,
                                 int        width,
                                 int        height,
                                 int        baseline)
{
  GtkWidget *child = gtk_widget_get_first_child (widget);

  if (child)
    {
      gtk_widget_measure (child, GTK_ORIENTATION_HORIZONTAL, width, NULL, NULL, NULL, NULL);
      gtk_widget_size_allocate (child, &(GtkAllocation) { 0, 0, width, height }, baseline);
    }
}

static gboolean
queue_alloc (gpointer user_data)
{
  gtk_widget_queue_allocate (GTK_WIDGET (user_data));
  return FALSE;
}

static void
remote_egl_widget_snapshot (GtkWidget *widget, GtkSnapshot *snapshot)
{
  RemoteEglWidget *ewidget = REMOTE_EGL_WIDGET (widget);
  int width, height;

  width = gtk_widget_get_width (GTK_WIDGET (widget));
  height = gtk_widget_get_height (GTK_WIDGET (widget));

  if (gtk_widget_get_first_child (widget))
    {
      GTK_WIDGET_CLASS (remote_egl_widget_parent_class)->snapshot (widget, snapshot);
      return;
    }

  remote_egl_widget_update_image (ewidget);

  if (gtk_widget_get_first_child (widget))
    g_idle_add_full (G_PRIORITY_DEFAULT, queue_alloc, g_object_ref (widget), g_object_unref);

  if (ewidget->texture)
    {
      graphene_rect_t bounds = GRAPHENE_RECT_INIT (0.f, 0.f, width, height);

      if (ewidget->swap_rb)
        gtk_snapshot_push_gl_shader (snapshot, ewidget->swap_shader, &bounds,
                                     g_bytes_new (NULL, 0));

      gtk_snapshot_append_texture (snapshot, ewidget->texture, &bounds);

      if (ewidget->swap_rb)
        {
          gtk_snapshot_gl_shader_pop_texture (snapshot);
          gtk_snapshot_pop (snapshot);
        }
    }
}

static void
remote_egl_widget_dispose (GObject *object)
{
  RemoteEglWidget *ewidget = REMOTE_EGL_WIDGET (object);
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))) != NULL)
    gtk_widget_unparent (child);

  g_clear_object (&ewidget->dbus_conn);
  g_clear_object (&ewidget->context);
  g_clear_object (&ewidget->message);
  g_clear_object (&ewidget->texture);
  g_clear_object (&ewidget->renderer);

  G_OBJECT_CLASS (remote_egl_widget_parent_class)->dispose (object);
}

static void
remote_egl_widget_finalize (GObject *object)
{
  RemoteEglWidget *ewidget = REMOTE_EGL_WIDGET (object);

  if (ewidget->tick_id)
    gtk_widget_remove_tick_callback (GTK_WIDGET (object), ewidget->tick_id);

  G_OBJECT_CLASS (remote_egl_widget_parent_class)->finalize (object);
}

static void
remote_egl_widget_class_init (RemoteEglWidgetClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  widget_class->realize = remote_egl_widget_realize;
  widget_class->unrealize = remote_egl_widget_unrealize;
  widget_class->size_allocate = remote_egl_widget_size_allocate;
  widget_class->snapshot = remote_egl_widget_snapshot;

  object_class->dispose = remote_egl_widget_dispose;
  object_class->finalize = remote_egl_widget_finalize;
}

typedef struct
{
  EGLDisplay   display;
  EGLImage     image;
  GUnixFDList *plane_fds;
} EGLTextureData;

static void
free_egl_texture_data (gpointer data)
{
  EGLTextureData *tdata = data;

  if (tdata->image != EGL_NO_IMAGE)
    eglDestroyImage (tdata->display, tdata->image);
  g_clear_object (&tdata->plane_fds);
  g_free (tdata);
}

static void
remote_egl_widget_update_image_egl (RemoteEglWidget *ewidget)
{
  guint32 width, height, fourcc, stride, offset;
  guint64 modifiers;
  g_autoptr (GVariantIter) planes = NULL;
  gint32 fd;
  int plane = 0;
  GUnixFDList *fd_list;
  const int *fds = NULL;
  int fd_count = 0;
  guint32 mod_lo = 0, mod_hi = 0;
  g_autoptr (GDBusMessage) message = NULL;
  EGLImage image;
  GLuint texid;
  EGLTextureData *texdata;
  g_autoptr (GdkTexture) texture = NULL;

  message = g_steal_pointer (&ewidget->message);

  g_variant_get (g_dbus_message_get_body (message),
                 "(uuuta(huu))",
                 &width, &height, &fourcc, &modifiers, &planes);
  fd_list = g_dbus_message_get_unix_fd_list (message);
  if (fd_list)
    fds = g_unix_fd_list_peek_fds (fd_list, &fd_count);

  mod_lo = modifiers & 0xFFFFFFFF;
  mod_hi = modifiers >> 32;
  if (modifiers != DRM_FORMAT_MOD_INVALID && modifiers != DRM_FORMAT_MOD_LINEAR && !ewidget->supports_modifiers)
    {
      remote_egl_widget_set_error_literal (ewidget,
                                           "Missing extension: EGL_EXT_image_dma_buf_import_modifiers");
      return;
    }

  g_autoptr (GArray) attribs = g_array_new (FALSE, FALSE, sizeof (EGLint));

  {
    EGLint new_attribs[] = {
      EGL_WIDTH,                width,
      EGL_HEIGHT,               height,
      EGL_LINUX_DRM_FOURCC_EXT, fourcc,
    };
    g_array_append_vals (attribs, new_attribs, G_N_ELEMENTS (new_attribs));
  }

  while (g_variant_iter_next (planes, "(huu)", &fd, &stride, &offset))
    {
      if (plane >= 4 || fd >= fd_count)
        break;

#define APPEND_PLANE(_fd, _offset, _pitch, _mod_lo, _mod_hi) \
      do \
        { \
          EGLint new_attribs[] = { \
            _fd, fds[fd], \
            _offset, offset, \
            _pitch, stride \
          }; \
          g_array_append_vals (attribs, new_attribs, G_N_ELEMENTS (new_attribs)); \
          if (ewidget->supports_modifiers) \
            { \
              EGLint mod_attribs[] = { \
                _mod_lo, mod_lo, \
                _mod_hi, mod_hi \
              }; \
              g_array_append_vals (attribs, mod_attribs, G_N_ELEMENTS (mod_attribs)); \
            } \
        } \
      while (FALSE)

      if (plane == 0)
        {
          APPEND_PLANE (EGL_DMA_BUF_PLANE0_FD_EXT,
                        EGL_DMA_BUF_PLANE0_OFFSET_EXT,
                        EGL_DMA_BUF_PLANE0_PITCH_EXT,
                        EGL_DMA_BUF_PLANE0_MODIFIER_LO_EXT,
                        EGL_DMA_BUF_PLANE0_MODIFIER_HI_EXT);
        }
      else if (plane == 1)
        {
          APPEND_PLANE (EGL_DMA_BUF_PLANE1_FD_EXT,
                        EGL_DMA_BUF_PLANE1_OFFSET_EXT,
                        EGL_DMA_BUF_PLANE1_PITCH_EXT,
                        EGL_DMA_BUF_PLANE1_MODIFIER_LO_EXT,
                        EGL_DMA_BUF_PLANE1_MODIFIER_HI_EXT);
        }
      else if (plane == 2)
        {
          APPEND_PLANE (EGL_DMA_BUF_PLANE2_FD_EXT,
                        EGL_DMA_BUF_PLANE2_OFFSET_EXT,
                        EGL_DMA_BUF_PLANE2_PITCH_EXT,
                        EGL_DMA_BUF_PLANE2_MODIFIER_LO_EXT,
                        EGL_DMA_BUF_PLANE2_MODIFIER_HI_EXT);
        }
      else if (ewidget->supports_modifiers && plane == 3)
        {
          APPEND_PLANE (EGL_DMA_BUF_PLANE3_FD_EXT,
                        EGL_DMA_BUF_PLANE3_OFFSET_EXT,
                        EGL_DMA_BUF_PLANE3_PITCH_EXT,
                        EGL_DMA_BUF_PLANE3_MODIFIER_LO_EXT,
                        EGL_DMA_BUF_PLANE3_MODIFIER_HI_EXT);
        }

#undef APPEND_PLANE

      plane++;
    }

  {
    EGLint end = EGL_NONE;
    g_array_append_val (attribs, end);
  }

  image = eglCreateImageKHR (ewidget->egl_display,
                             EGL_NO_CONTEXT,
                             EGL_LINUX_DMA_BUF_EXT,
                             NULL,
                             (const EGLint *) attribs->data);
  if (!image)
    {
      g_critical ("eglCreateImageKHR: %s", egl_error_str ());
      return;
    }

  texdata = g_new0 (EGLTextureData, 1);
  texdata->display = ewidget->egl_display;
  texdata->image = image;
  g_set_object (&texdata->plane_fds, fd_list);

  gdk_gl_context_make_current (ewidget->context);
  glGenTextures (1, &texid);
  glBindTexture (GL_TEXTURE_2D, texid);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glEGLImageTargetTexture2DOES (GL_TEXTURE_2D, image);
  glBindTexture (GL_TEXTURE_2D, 0);

  texture = gdk_gl_texture_new (ewidget->context, texid, width, height, free_egl_texture_data, texdata);
  g_set_object (&ewidget->texture, texture);
}

static uint32_t
depth_for_format (uint32_t format)
{
  switch (format)
    {
    case DRM_FORMAT_XRGB1555:
    case DRM_FORMAT_XBGR1555:
      return 15;
    case DRM_FORMAT_RGB565:
    case DRM_FORMAT_BGR565:
      return 16;
    case DRM_FORMAT_XRGB8888:
    case DRM_FORMAT_XBGR8888:
      return 24;
    case DRM_FORMAT_XRGB2101010:
    case DRM_FORMAT_XBGR2101010:
      return 30;
    case DRM_FORMAT_ARGB8888:
    case DRM_FORMAT_ABGR8888:
      return 32;
    case DRM_FORMAT_ARGB16161616F:
    case DRM_FORMAT_ABGR16161616F:
      return 64;
    default:
      return 0;
    }
}

static uint32_t
bpp_for_format (uint32_t format) {
  switch (format)
    {
    case DRM_FORMAT_XRGB1555:
    case DRM_FORMAT_XBGR1555:
    case DRM_FORMAT_RGB565:
    case DRM_FORMAT_BGR565:
      return 2;
    case DRM_FORMAT_XRGB8888:
    case DRM_FORMAT_XBGR8888:
    case DRM_FORMAT_XRGB2101010:
    case DRM_FORMAT_XBGR2101010:
    case DRM_FORMAT_ARGB8888:
    case DRM_FORMAT_ABGR8888:
      return 4;
    case DRM_FORMAT_ARGB16161616F:
    case DRM_FORMAT_ABGR16161616F:
      return 8;
    default:
      return 0;
    }
}

static gboolean
swapped_for_format (uint32_t format)
{
  switch (format)
    {
    case DRM_FORMAT_XBGR1555:
    case DRM_FORMAT_BGR565:
    case DRM_FORMAT_XBGR8888:
    case DRM_FORMAT_XBGR2101010:
    case DRM_FORMAT_ABGR8888:
    case DRM_FORMAT_ABGR16161616F:
      return TRUE;
    default:
      return FALSE;
    }
}

typedef struct
{
  xcb_connection_t *conn;
  Display *display;
  Pixmap pixmap;
  GLXPixmap glxpixmap;
} GLXTextureData;

static void
free_glx_texture_data (gpointer data)
{
  GLXTextureData *tdata = data;

  glXReleaseTexImageEXT (tdata->display, tdata->glxpixmap, GLX_FRONT_LEFT_EXT);
  glXDestroyGLXPixmap (tdata->display, tdata->glxpixmap);
  xcb_free_pixmap (tdata->conn, tdata->pixmap);
  g_free (tdata);
}

static void
remote_egl_widget_update_image_glx (RemoteEglWidget *ewidget)
{
  g_autoptr (GDBusMessage) message = NULL;
  guint32 width, height, fourcc, stride, offset;
  guint64 modifiers;
  g_autoptr (GVariantIter) planes = NULL;
  gint32 fd;
  int plane = 0;
  GUnixFDList *fd_list;
  g_autofree int *fds = NULL;
  int fd_count = 0;
  guint32 strides[4] = { 0, };
  guint32 offsets[4] = { 0, };
  int fds_out[4] = { -1, -1, -1, -1 };
  xcb_connection_t *conn;
  GdkSurface *surface;
  Window win;
  guint depth, bpp;
  xcb_void_cookie_t cookie;
  Pixmap pixmap;
  GLXPixmap glxpixmap;
  GLuint texid;
  GLXTextureData *texdata;
  g_autoptr (GdkTexture) texture = NULL;
  const int pixmap_attribs[] = {
    GLX_TEXTURE_TARGET_EXT, GLX_TEXTURE_2D_EXT,
    GLX_TEXTURE_FORMAT_EXT, GLX_TEXTURE_FORMAT_RGBA_EXT,
    None
  };

  message = g_steal_pointer (&ewidget->message);

  g_variant_get (g_dbus_message_get_body (message),
                 "(uuuta(huu))",
                 &width, &height, &fourcc, &modifiers, &planes);
  depth = depth_for_format (fourcc);
  bpp = bpp_for_format (fourcc) * 8;

  if (!depth || !bpp)
    {
      remote_egl_widget_set_error_literal (ewidget,
                                           "Unsupported buffer format 0x%08x",
                                           fourcc);
      return;
    }

  conn = XGetXCBConnection (ewidget->x11_display);
  surface
    = gtk_native_get_surface (GTK_NATIVE (gtk_widget_get_root (GTK_WIDGET (ewidget))));
  win = gdk_x11_surface_get_xid (surface);

  gdk_gl_context_make_current (ewidget->context);

  if (ewidget->x11_fb_config == NULL || depth != ewidget->x11_fb_depth)
    {
      GLXFBConfig *configs;
      int num_configs;
      GLXFBConfig config = NULL;
      const int config_attribs[] = {
          GLX_BIND_TO_TEXTURE_RGBA_EXT,    GL_TRUE,
          GLX_BIND_TO_TEXTURE_TARGETS_EXT, GLX_TEXTURE_2D_BIT_EXT,
          GLX_DOUBLEBUFFER,                GL_FALSE,
          GLX_DRAWABLE_TYPE,               GLX_PIXMAP_BIT,
          None
      };

      configs = glXChooseFBConfig (ewidget->x11_display, ewidget->x11_screen,
                                   config_attribs, &num_configs);

      for (int i = 0; i < num_configs; i++)
        {
          GLXFBConfig c = configs[i];
          XVisualInfo *visual = glXGetVisualFromFBConfig (ewidget->x11_display, c);
          gboolean found = visual && visual->depth == depth;
          XFree (visual);

          if (!found)
            continue;
          config = c;
          break;
        }

      XFree (configs);

      if (config == NULL)
        {
          remote_egl_widget_set_error_literal (ewidget,
                                               "No compatible GLXFBConfig found for depth %d",
                                               depth);
          return;
        }

      ewidget->x11_fb_config = config;
      ewidget->x11_fb_depth = depth;
    }

  fd_list = g_dbus_message_get_unix_fd_list (message);
  if (fd_list)
    fds = g_unix_fd_list_steal_fds (fd_list, &fd_count);

  while (g_variant_iter_next (planes, "(huu)", &fd, &stride, &offset))
    {
      if (plane >= 4 || fd >= fd_count)
        break;
      fds_out[plane] = fds[fd];
      strides[plane] = stride;
      offsets[plane] = offset;
      plane++;
    }

  pixmap = xcb_generate_id (conn);

  if ((modifiers != DRM_FORMAT_MOD_INVALID && modifiers != DRM_FORMAT_MOD_LINEAR) || plane > 1)
    cookie =
       xcb_dri3_pixmap_from_buffers_checked (conn, pixmap, win, plane,
                                             width, height,
                                             strides[0], offsets[0],
                                             strides[1], offsets[1],
                                             strides[2], offsets[2],
                                             strides[3], offsets[3],
                                             depth, bpp, modifiers, fds_out);
  else
    cookie =
       xcb_dri3_pixmap_from_buffer_checked (conn, pixmap, win,
                                            height * strides[0],
                                            width, height, strides[0],
                                            depth, bpp, fds_out[0]);

  xcb_discard_reply (conn, cookie.sequence);

  glxpixmap = glXCreatePixmap (ewidget->x11_display, ewidget->x11_fb_config,
                               pixmap, pixmap_attribs);

  texdata = g_new0 (GLXTextureData, 1);
  texdata->conn = conn;
  texdata->display = ewidget->x11_display;
  texdata->pixmap = pixmap;
  texdata->glxpixmap = glxpixmap;

  glGenTextures (1, &texid);
  glBindTexture (GL_TEXTURE_2D, texid);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glXBindTexImageEXT (ewidget->x11_display, texdata->glxpixmap, GLX_FRONT_LEFT_EXT, NULL);
  glBindTexture (GL_TEXTURE_2D, 0);

  texture = gdk_gl_texture_new (ewidget->context, texid, width, height,
                                (GDestroyNotify) free_glx_texture_data, texdata);
  g_set_object (&ewidget->texture, texture);
  ewidget->swap_rb = swapped_for_format (fourcc);
}

static void
remote_egl_widget_update_image (RemoteEglWidget *ewidget)
{
  if (!ewidget->message)
    return;
  if (ewidget->label)
    return;
  if (ewidget->egl_display)
    remote_egl_widget_update_image_egl (ewidget);
  else if (ewidget->x11_display)
    remote_egl_widget_update_image_glx (ewidget);
}

static void
build_ui (GtkApplication *app)
{
  GtkWidget *window;
  GtkWidget *ewidget;

  if (gtk_application_get_windows (app) != NULL)
    return;

  window = gtk_application_window_new (app);
  gtk_window_set_default_size (GTK_WINDOW (window), 400, 400);
  ewidget = g_object_new (REMOTE_EGL_WIDGET_TYPE, NULL);
  gtk_window_set_child (GTK_WINDOW (window), ewidget);
  gtk_window_present (GTK_WINDOW (window));
}

static void
set_exe_dir (const char *argv0)
{
  g_autofree char *exe = g_file_read_link ("/proc/self/exe", NULL);
  if (exe)
    exe_dir = g_path_get_dirname (exe);
  else if (argv0 && argv0[0])
    exe_dir = g_path_get_dirname (argv0);
}

int
main (int argc, char *argv[])
{
  g_autoptr (GtkApplication) app = NULL;

  set_exe_dir (argc > 0 ? argv[0] : NULL);

  app = gtk_application_new (APP_NAME, G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (build_ui), NULL);
  return g_application_run (G_APPLICATION (app), argc, argv);
}
