#pragma once

#include <epoxy/egl.h>
#include <glib.h>

static const char *
egl_error_str (void)
{
  GLint e = eglGetError ();
#define CHECK(_err) \
  do \
    if (e == _err) \
      return #_err; \
  while (0)

  CHECK (EGL_SUCCESS);
  CHECK (EGL_NOT_INITIALIZED);
  CHECK (EGL_BAD_ACCESS);
  CHECK (EGL_BAD_ALLOC);
  CHECK (EGL_BAD_ATTRIBUTE);
  CHECK (EGL_BAD_CONTEXT);
  CHECK (EGL_BAD_CONFIG);
  CHECK (EGL_BAD_CURRENT_SURFACE);
  CHECK (EGL_BAD_DISPLAY);
  CHECK (EGL_BAD_SURFACE);
  CHECK (EGL_BAD_MATCH);
  CHECK (EGL_BAD_PARAMETER);
  CHECK (EGL_BAD_NATIVE_PIXMAP);
  CHECK (EGL_BAD_NATIVE_WINDOW);
  CHECK (EGL_CONTEXT_LOST);
  CHECK (EGL_BAD_STREAM_KHR);
  CHECK (EGL_BAD_STATE_KHR);
  CHECK (EGL_BAD_DEVICE_EXT);
  CHECK (EGL_BAD_OUTPUT_LAYER_EXT);
  CHECK (EGL_BAD_OUTPUT_PORT_EXT);

#undef CHECK
  static char buf[64];
  g_snprintf (buf, sizeof buf, "Unknown error 0x%08x", e);
  return buf;
}

